<?php

/**
 * @file
 * Ubercart Order Complete
 */

/*************************************************************************
* Ubercart hooks
*************************************************************************/

/**
 * Implementation of hook_order()
 */
function uc_order_complete_order($op, &$order, $order_status) {

  // Switch through the various operations.
  switch ($op) {

    // When an order is loaded...
    case 'load':

      // Get the completion time (if available), and add it to the order object.
      $order->completed = uc_order_complete_get($order->order_id);

      break;

    // When an order is updated...
    case 'update':

      // If the order status is being changed...
      if ($order->order_status != $order_status) {

        // If the order status is being changed to 'completed'...
        if ($order_status == 'completed') {

          // Set the completion timestamp to the current time.
          $order->completed = uc_order_complete_set($order->order_id);

          // Invoke hook_uc_order_complete() so other modules can take action.
          module_invoke_all('uc_order_complete', $order);
        }

        // Or, if the order status is being changed FROM 'completed' to something else...
        elseif ($order->order_status == 'completed') {

          // Delete the order completion time from the database.
          uc_order_complete_delete($order->order_id);

          // Set the completed time to FALSE and invoke hook_uc_order_uncomplete() so other modules can take action.
          $order->completed = FALSE;
          module_invoke_all('uc_order_uncomplete', $order);
        }
      }

      break;

    // When an order id deleted...
    case 'delete':

      // Delete the order completion time from the database.
      uc_order_complete_delete($order->order_id);

      break;
  }
}

/*************************************************************************
* Helper functions
*************************************************************************/

/**
 * Get the completed timestamp for an order.
 *
 * @param $order_id
 *   The order id to look up.
 *
 * @return
 *   Returns the timestamp when the order was marked 'complete', FALSE if a record doesn't exist.
 */
function uc_order_complete_get($order_id) {
  return db_result(db_query('SELECT timestamp FROM {uc_order_complete} WHERE order_id=%d', $order_id));
}

/**
 * Set the completed timestamp for an order.
 *
 * @param $order_id
 *   The order id to set.
 * @param $timestamp
 *   The timestamp to set. If NULL, then the current time will be used.
 *
 * @return
 *   Returns the new timestamp, if successful, FALSE otherwise.
 */
function uc_order_complete_set($order_id, $timestamp = NULL) {

  // If the timestamp is NULL, use the current time.
  if (is_null($timestamp)) {
    $timestamp = time();
  }

  // Check to see if a record already exists.
  $exists = uc_order_complete_get($order_id);
  if ($exists === FALSE) {
    $success = db_query('INSERT INTO {uc_order_complete} (order_id, timestamp) VALUES(%d, %d)', $order_id, $timestamp);
  } else {
    $success = db_query('UPDATE {uc_order_complete} SET timestamp=%d WHERE order_id=%d', $timestamp, $order_id);
  }

  // If the query was successful, return the timestamp. Otherwise return FALSE.
  if ($success) {
    return $timestamp;
  }
  else {
    return FALSE;
  }
}

/**
 * Delete the completed timestamp for an order.
 *
 * @param $order_id
 *   The order id to delete.
 *
 * @return
 *   Returns the result of db_query.
 */
function uc_order_complete_delete($order_id) {
  return db_query('DELETE FROM {uc_order_complete} WHERE order_id=%d', $order_id);
}
