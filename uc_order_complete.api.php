<?php

/**
 * @file
 * Hooks provided by UC Order Complete.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Act on orders when they are marked 'completed'.
 *
 * @param $order
 *   The order object.
 */
function hook_uc_order_complete($order) {

  // Do stuff...
}

/**
 * Act on orders when they are changed from 'completed' to something else.
 *
 * @param $order
 *   The order object.
 */
function hook_uc_order_uncomplete($order) {

  // Do stuff...
}

/**
 * @} End of "addtogroup hooks".
 */