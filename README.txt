---UC Order Complete---
by m.stenta (drupal.org/user/581414)

This module provides the ability to perform actions when an order is marked 'completed'.

Out of the box, it provides two simple features:

1. It records the timestamp when an order's status is changed to 'completed'.
2. It provides two hooks allowing other modules to do stuff when an order's completion date is set/unset.

--Completed timestamp--

When an order's status is changed to 'completed', this module will store the current timestamp in the {uc_order_complete} table.

This timestamp is available on the $order object in $order->completed.

--Hooks--

Modules can act upon an order at the time of it's completion by invoking hook_uc_order_complete().

They can also act upon an order if the status is changed FROM 'completed' to something else, by invoking hook_uc_order_uncomplete().

See uc_order_complete.api.php included with this module for usage details.